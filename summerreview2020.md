# The Summer in Review: June-July-August 2020

Hello everyone.

Welcome to a review of the developent of OpenMW during the summer of 2020. A very unusual summer period for most of us. Development has been as stable as anytime though, so let's dig in.

The big thing for the month of June was the merging of Object Paging, covered in the last ["OpenMW Spotlight"](https://openmw.org/2020/openmw-spotlight-turning-the-pages) post. We will not cover this feature any more today since we already did an in-depth post about it. Without further ado, allow us to present the goodies!

- 3rd person view has seen [lots of improvements](https://gitlab.com/OpenMW/openmw/-/merge_requests/219) lately thanks to Petr Mikheev and he has continued to work on this feature and more, including a more [modern vanity camera feature](https://gitlab.com/OpenMW/openmw/-/merge_requests/261), during the last months.
- Capostrophic has, other than graduating high school with amazing grades, been bug fixing as usual. He also implemented some [nice shader fixes](https://github.com/OpenMW/openmw/pull/2901) and some [nif](https://github.com/OpenMW/openmw/pull/2883) [format](https://github.com/OpenMW/openmw/pull/2908) [improvements](https://github.com/OpenMW/openmw/pull/2987). 
- akortunov has been bugfixing like crazy, especially during the month of June, and refactored quite a few parts of the codebase. Lately, he has been working on making his old [grass handling](https://github.com/OpenMW/openmw/pull/2665) pull request play well with our new object paging feature.
- elsid has, other than maintaining the AI navigation feature of the engine, been fixing various sound issues. He also has an open pull request which is supposed to make the engine load [sound files in a separate thread](https://github.com/OpenMW/openmw/pull/2928).
- unelsson and psi29a have been working together on making OpenMW support [animations when using the Dae/Collada 3d model format](https://gitlab.com/OpenMW/openmw/-/merge_requests/252). This is a very important task to be done if this engine is ever going to become the FLOSS RPG game engine it strives to be. Psi29a started doing the research on how the feature should be implemented, and nelsson continued his work. Moreover, we should also mention that nelsson sent a few pull requests directly to our dependency used for rendering, OpenSceneGraph, to improve Dae/Collada support even more.
- fr3dz10's old [merge request](https://gitlab.com/OpenMW/openmw/-/merge_requests/126) for color changes in keywords in dialogue window was merged in June. He has also been [experimenting with the physics engine](https://gitlab.com/OpenMW/openmw/-/merge_requests/248), trying to make the physics run in a separate thread instead of the main thread. Really interesting!
- wareya has been working on the movement solver for quite some time, trying to emulate the movement from vanilla Morrowind as much as possible. During the past months, he started doing a [rewrite](https://github.com/OpenMW/openmw/pull/2937) of his older pull request. This work should be ready quite soon.
- Mads Buvik Sandvei, aka Foal, is working on a *very* interesting feature, namely support for VR. He has shown great progress already. You can follow his progress on his merge request [here](https://gitlab.com/OpenMW/openmw/-/merge_requests/251).

This is just a short review of what has been going on during the summer. There has been a lot more going on of course, but we tried to sum up the more interesting changes.

Thank you for reading, and see you next time!
